# Specs
http://jean-luc.massat.perso.luminy.univ-amu.fr/ens/jee/tp-JSF.html

## Parts skipped to do the "Traitement AJAX" part first
- Injecter des données dans un formulaire
- Création d'un enseignement
- Définir ses propres tags
- Approfondissements

# TomEE setup (for Eclipse)
## Download Tom EE Plus
https://tomee.apache.org/downloads.html

Here we used version 1.7.4 Plus (NOT JavaEE6 certified)

## Add a new server runtime: Apache Tomcat 7.x
Set the Tomcat install directory to the one were you extracted TomEE

## Add a new server which uses this runtime
