package monapp;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

@SuppressWarnings("serial")
@ManagedBean()
@ViewScoped
public class ViewCounter implements Serializable {

    @ManagedProperty("#{applicationCounter}")
    ApplicationCounter appCounter;

    int value = 0;

    public Integer getCounter() {
        // called twice by MyFaces for some reason, that's why the counter is
        // incremented twice
        return ++value;
    }

    public ApplicationCounter getAppCounter() {
        return appCounter;
    }

    public void setAppCounter(ApplicationCounter appCounter) {
        this.appCounter = appCounter;
    }

    @PostConstruct
    void init() {
        System.err.println("Create " + this + " appCounter: " + appCounter);
    }

    @PreDestroy
    void close() {
        System.err.println("Close " + this);
    }
    
}