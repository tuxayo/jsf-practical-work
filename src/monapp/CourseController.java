package monapp;

import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "course")
@SessionScoped
public class CourseController {

    @EJB
    CourseManager cm;
    
    @ManagedProperty("#{messages}")
    private ResourceBundle bundle;

    Course theCourse = new Course();

    @PostConstruct
    public void init() {
        System.out.println("Create " + this);
        if (cm.findCourses().size() == 0) {
            Course c1 = new Course();
            c1.setName("Architecture JEE");
            c1.setHours(60);
            c1.setDescription("Introduction à JEE.");
            cm.saveCourse(c1);
        }
    }

    public List<Course> getCourses() {
        return cm.findCourses();
    }

    public Course getTheCourse() {
        return theCourse;
    }

    public String show(Integer n) {
        theCourse = cm.findCourse(n);
        return "showCourse";
    }

    public String save() {
        System.out.println("Calling save() on course: " + theCourse);
        if (theCourse.getHours() % 3 != 0) {
            FacesContext ct = FacesContext.getCurrentInstance();
            FacesMessage msg = new FacesMessage(bundle.getString("course.validator.hours.not_multiple_of_3"));
            ct.addMessage("test:hours", msg);
            ct.validationFailed();
            return "editCourse";
        }

        cm.saveCourse(theCourse);
        return "showCourse";
    }

    public String newCourse() {
        theCourse = new Course();
        return "editCourse";
    }
    
    public void setBundle(ResourceBundle bundle) {
        this.bundle = bundle;
    }

}